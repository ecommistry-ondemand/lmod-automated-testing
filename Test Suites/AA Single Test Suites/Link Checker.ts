<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description>Checks magento page links</description>
   <name>Link Checker</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2018-09-12T12:25:56</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>24ca3b8f-6f12-4dc3-86bd-190131ebbefe</testSuiteGuid>
   <testCaseLink>
      <guid>62c0e69a-8282-49a8-992c-c937e54bb0f8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/UK - Link Checker</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
