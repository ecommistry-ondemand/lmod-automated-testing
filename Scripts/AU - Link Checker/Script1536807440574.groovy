import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.maximizeWindow()

'https://www.lesmillsondemand.com/en_us/schedules/'
WebUI.navigateToUrl(GlobalVariable.SiteURL)

WebUI.waitForPageLoad(30)

WebUI.click(findTestObject('Page_Browse - LES MILLS ON DEMAND/a_FAQ'))

WebUI.waitForPageLoad(30)

WebUI.takeScreenshot()

'Sign In'
WebUI.click(findTestObject('Page_LES MILLS On Demand - Faq/a_Sign in'))

WebUI.waitForPageLoad(30)

WebUI.takeScreenshot()

WebUI.focus(findTestObject('Page_LES MILLS On Demand - Sign in/input_loginusername'))

'Canceled Account Login Username'
WebUI.setText(findTestObject('Page_LES MILLS On Demand - Sign in/input_loginusername'), GlobalVariable.CanceledLoginUserName)

WebUI.focus(findTestObject('Page_LES MILLS On Demand - Sign in/input_loginpassword'))

'Canceled Account Login Password'
WebUI.setText(findTestObject('Page_LES MILLS On Demand - Sign in/input_loginpassword'), GlobalVariable.CanceledLoginPassword)

WebUI.click(findTestObject('Page_LES MILLS On Demand - Sign in/button_Sign in'))

'https://www.lesmillsondemand.com/en_us/faq/'
WebUI.navigateToUrl(GlobalVariable.FAQ, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.takeScreenshot()

'https://www.lesmillsondemand.com/en_us/customer/account/'
WebUI.navigateToUrl(GlobalVariable.CustomerAccount, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.takeScreenshot()

'https://www.lesmillsondemand.com/en_us/subscriptions/'
WebUI.navigateToUrl(GlobalVariable.Subscriptions, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.takeScreenshot()

'https://www.lesmillsondemand.com/en_us/subscriptions/friend/'
WebUI.navigateToUrl(GlobalVariable.Friend, FailureHandling.OPTIONAL)

WebUI.takeScreenshot()

'https://www.lesmillsondemand.com/en_us/gift_subscriptions/'
WebUI.navigateToUrl(GlobalVariable.Gift, FailureHandling.OPTIONAL)

WebUI.takeScreenshot()

WebUI.closeBrowser()

