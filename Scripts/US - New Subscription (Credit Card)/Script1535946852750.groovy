import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.logging.KeywordLogger as KeywordLogger
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW

WebUI.openBrowser('')

WebUI.maximizeWindow()

WebUI.navigateToUrl(GlobalVariable.SiteURL)

WebUI.waitForPageLoad(30)

WebUI.takeScreenshot()

WebUI.click(findTestObject('Page_Browse - LES MILLS ON DEMAND/a_FAQ'))

WebUI.waitForPageLoad(30)

WebUI.takeScreenshot()

WebUI.click(findTestObject('Object Repository/Page_LES MILLS On Demand - Faq/a_Subscribe'))

WebUI.click(findTestObject('Page_LES MILLS On Demand  At Home W/a_START YOUR FREE TRIAL'))

WebUI.switchToWindowTitle('LES MILLS On Demand - Membership')

WebUI.takeScreenshot()

WebUI.click(findTestObject('Object Repository/Page_LES MILLS On Demand - Membersh/a_Start free trial'))

WebUI.waitForPageLoad(30)

WebUI.setText(findTestObject('Object Repository/Page_LES MILLS On Demand/input_billingfirstname'), 'Ecommistry')

WebUI.setText(findTestObject('Object Repository/Page_LES MILLS On Demand/input_billinglastname'), 'Test')

WebUI.focus(findTestObject('Page_LES MILLS On Demand/input_billingemail'))

'Add email address + date & time + @ecommistry.com'
CustomKeywords.'dateTime.dateTimeKeyword.emailDateTimeGenerator'()

WebUI.sendKeys(findTestObject('Page_LES MILLS On Demand/Checkbox By Logging in you agree'), Keys.chord(Keys.SPACE))

WebUI.setText(findTestObject('Object Repository/Page_LES MILLS On Demand/input_billingcustomer_password'), 'ecommistrytest')

WebUI.takeScreenshot()

WebUI.click(findTestObject('Page_LES MILLS On Demand/button_Continue'))

WebUI.selectOptionByIndex(findTestObject('Les Mills US payment Form/Page_LES MILLS On Demand/select_United States'), '1')

WebUI.selectOptionByIndex(findTestObject('Page_LES MILLS On Demand/select_Please select state'), '1')

WebUI.setText(findTestObject('Les Mills US payment Form/Page_LES MILLS On Demand/input__billingcity'), 'Alabama')

WebUI.setText(findTestObject('Les Mills US payment Form/Page_LES MILLS On Demand/input__billingpostcode'), 'Wc001')

WebUI.setText(findTestObject('Les Mills US payment Form/Page_LES MILLS On Demand/input__credit-card-number'), GlobalVariable.ccNumber)

WebUI.setText(findTestObject('Les Mills US payment Form/Page_LES MILLS On Demand/input__expiration-date'), GlobalVariable.ccExpireDate)

WebUI.setText(findTestObject('Les Mills US payment Form/Page_LES MILLS On Demand/input__cvv'), GlobalVariable.ccCCV)

WebUI.delay(2)

WebUI.takeScreenshot()

WebUI.click(findTestObject('Object Repository/Page_LES MILLS On Demand/button_Continue'))

WebUI.delay(15)

WebUI.scrollToElement(findTestObject('Page_LES MILLS On Demand/button_Buy Now'), 30)

WebUI.click(findTestObject('Object Repository/Page_LES MILLS On Demand/label_I AM OVER 18 AND I AGREE'), FailureHandling.STOP_ON_FAILURE)

WebUI.takeScreenshot()

WebUI.click(findTestObject('Object Repository/Page_LES MILLS On Demand/button_Buy Now'))

WebUI.delay(15)

'Success Screen'
WebUI.takeScreenshot()

WebUI.navigateToUrl(GlobalVariable.Browse)

WebUI.scrollToElement(findTestObject('Page_Browse - LES MILLS ON DEMAND/div_BODYCOMBAT'), 30)

WebUI.click(findTestObject('Page_Browse - LES MILLS ON DEMAND/div_BODYCOMBAT'))

WebUI.waitForPageLoad(30)

WebUI.delay(15)

WebUI.takeScreenshot()

WebUI.navigateToUrl(GlobalVariable.Browse)

WebUI.scrollToElement(findTestObject('Page_Browse - LES MILLS ON DEMAND/div_BODYPUMP'), 30)

WebUI.click(findTestObject('Page_Browse - LES MILLS ON DEMAND/div_BODYPUMP'))

WebUI.delay(15)

WebUI.takeScreenshot()

WebUI.navigateToUrl('https://www.lesmillsondemand.com/en_uk/customer/account/')

WebUI.click(findTestObject('Object Repository/Page_LES MILLS On Demand - My Accou/span_Cancel'))

WebUI.click(findTestObject('Object Repository/Page_LES MILLS On Demand - My Subsc/span_CANCEL LES MILLS ON DEMAN'))

WebUI.acceptAlert()

WebUI.navigateToUrl('https://www.lesmillsondemand.com/en_us/customer/account/')

WebUI.takeScreenshot()

WebUI.delay(2)

WebUI.closeBrowser()

