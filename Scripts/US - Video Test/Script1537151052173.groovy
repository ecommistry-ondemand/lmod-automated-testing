import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW

WebUI.openBrowser('')

WebUI.maximizeWindow()

'https://www.lesmillsondemand.com/en_us/schedules/'
WebUI.navigateToUrl(GlobalVariable.SiteURL)

WebUI.waitForPageLoad(30)

WebUI.click(findTestObject('Page_Browse - LES MILLS ON DEMAND/a_FAQ'))

WebUI.waitForPageLoad(30)

WebUI.takeScreenshot()

'Sign In'
WebUI.click(findTestObject('Page_LES MILLS On Demand - Faq/a_Sign in'))

WebUI.waitForPageLoad(30)

WebUI.takeScreenshot()

WebUI.focus(findTestObject('Page_LES MILLS On Demand - Sign in/input_loginusername'))

'Active Account Login Username'
WebUI.setText(findTestObject('Page_LES MILLS On Demand - Sign in/input_loginusername'), GlobalVariable.ActiveLoginUserName)

WebUI.focus(findTestObject('Page_LES MILLS On Demand - Sign in/input_loginpassword'))

'Active Account Login Password'
WebUI.setText(findTestObject('Page_LES MILLS On Demand - Sign in/input_loginpassword'), GlobalVariable.ActiveLoginPassword)

WebUI.click(findTestObject('Page_LES MILLS On Demand - Sign in/button_Sign in'))

'Live Browse'
WebUI.navigateToUrl(GlobalVariable.Browse, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.scrollToElement(findTestObject('Page_Browse - LES MILLS ON DEMAND/div_BODYCOMBAT'), 30)

WebUI.click(findTestObject('Page_Browse - LES MILLS ON DEMAND/div_BODYCOMBAT'))

WebUI.waitForPageLoad(30)

WebUI.delay(15)

WebUI.takeScreenshot()

WebUI.navigateToUrl(GlobalVariable.Browse)

WebUI.scrollToElement(findTestObject('Page_Browse - LES MILLS ON DEMAND/div_BODYPUMP'), 30)

WebUI.click(findTestObject('Page_Browse - LES MILLS ON DEMAND/div_BODYPUMP'))

WebUI.waitForPageLoad(30)

WebUI.delay(15)

WebUI.takeScreenshot()

WebUI.closeBrowser()

