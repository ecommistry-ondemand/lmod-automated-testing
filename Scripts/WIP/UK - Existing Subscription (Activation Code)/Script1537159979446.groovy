import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import javax.swing.JFrame as JFrame
import javax.swing.JOptionPane as JOptionPane
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

JFrame frame = new JFrame('User Input Frame')

frame.requestFocus()

String activCode = JOptionPane.showInputDialog(frame, 'Enter Activation Code')

WebUI.maximizeWindow()

'https://www.lesmillsondemand.com/en_us/schedules/'
WebUI.navigateToUrl(GlobalVariable.SiteURL)

WebUI.waitForPageLoad(30)

WebUI.click(findTestObject('Page_Browse - LES MILLS ON DEMAND/a_FAQ'))

WebUI.waitForPageLoad(30)

WebUI.takeScreenshot()

WebUI.click(findTestObject('Page_LES MILLS On Demand - Faq/span_Close'))

'Sign In'
WebUI.click(findTestObject('Page_LES MILLS On Demand - Faq/a_Sign in'))

WebUI.waitForPageLoad(30)

WebUI.takeScreenshot()

WebUI.focus(findTestObject('Page_LES MILLS On Demand - Sign in/input_loginusername'))

'Canceled Account Login Username'
WebUI.setText(findTestObject('Page_LES MILLS On Demand - Sign in/input_loginusername'), GlobalVariable.CanceledLoginUserName)

WebUI.focus(findTestObject('Page_LES MILLS On Demand - Sign in/input_loginpassword'))

'Canceled Account Login Password'
WebUI.setText(findTestObject('Page_LES MILLS On Demand - Sign in/input_loginpassword'), GlobalVariable.CanceledLoginPassword)

WebUI.click(findTestObject('Page_LES MILLS On Demand - Sign in/button_Sign in'))

'https://www.lesmillsondemand.com/en_us/faq/'
WebUI.navigateToUrl(GlobalVariable.FAQ, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Page_LES MILLS On Demand - Faq/a_Subscribe'))

WebUI.waitForPageLoad(30)

WebUI.click(findTestObject('Page_LES MILLS On Demand - Membersh/a_Start free trial'))

WebUI.waitForPageLoad(30)

WebUI.verifyTextPresent('david.apthorp@ecommistry.com', false)

WebUI.sendKeys(findTestObject('Page_LES MILLS On Demand/Checkbox By Logging in you agree'), Keys.chord(Keys.SPACE))

WebUI.takeScreenshot()

WebUI.click(findTestObject('Page_LES MILLS On Demand/button_Continue'))

WebUI.delay(5)

WebUI.click(findTestObject('Page_LES MILLS On Demand/div_I have an activation code'))

WebUI.setText(findTestObject('Page_LES MILLS On Demand/input_I have an activation cod'), activCode)

WebUI.click(findTestObject('Page_LES MILLS On Demand/button_Apply'))

WebUI.check(findTestObject('Page_LES MILLS On Demand/button_Continue'))

WebUI.delay(5)

WebUI.scrollToElement(findTestObject('Page_LES MILLS On Demand/button_Buy Now'), 30)

WebUI.click(findTestObject('Page_LES MILLS On Demand/label_I AM OVER 18 AND I AGREE'))

WebUI.takeScreenshot()

WebUI.click(findTestObject('Page_LES MILLS On Demand/button_Buy Now'))

WebUI.delay(15)

WebUI.takeScreenshot()

WebUI.navigateToUrl(GlobalVariable.Browse)

WebUI.scrollToElement(findTestObject('Page_Browse - LES MILLS ON DEMAND/div_BODYCOMBAT'), 30)

WebUI.click(findTestObject('Page_Browse - LES MILLS ON DEMAND/div_BODYCOMBAT'))

WebUI.waitForPageLoad(30)

WebUI.delay(15)

WebUI.takeScreenshot()

WebUI.navigateToUrl(GlobalVariable.Browse)

WebUI.scrollToElement(findTestObject('Page_Browse - LES MILLS ON DEMAND/div_BODYPUMP'), 30)

WebUI.click(findTestObject('Page_Browse - LES MILLS ON DEMAND/div_BODYPUMP'))

WebUI.waitForPageLoad(0)

WebUI.delay(15)

WebUI.takeScreenshot()

WebUI.navigateToUrl('https://www.lesmillsondemand.com/en_uk/customer/account/')

WebUI.click(findTestObject('Page_LES MILLS On Demand - My Accou/span_Cancel'))

WebUI.click(findTestObject('Page_LES MILLS On Demand - My Subsc/span_CANCEL LES MILLS ON DEMAN'))

WebUI.acceptAlert()

WebUI.navigateToUrl('https://www.lesmillsondemand.com/en_uk/customer/account/')

WebUI.takeScreenshot()

WebUI.delay(2)

WebUI.closeBrowser()

