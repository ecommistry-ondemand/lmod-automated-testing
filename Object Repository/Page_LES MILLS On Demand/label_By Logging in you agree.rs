<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>label_By Logging in you agree</name>
   <tag></tag>
   <elementGuidId>80bc087c-4a0e-483d-92ec-2df3d593cd92</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value></value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//label[(text() = 'By Logging in, you agree to the                          
                              Les Mills On Demand Privacy Policy.                          
                      ' or . = 'By Logging in, you agree to the                          
                              Les Mills On Demand Privacy Policy.                          
                      ')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>label</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>for</name>
      <type>Main</type>
      <value>privacy_policy</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>By Logging in, you agree to the                          
                              Les Mills On Demand Privacy Policy.                          
                      </value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;co-billing-form&quot;)/div[@class=&quot;checkbox-area&quot;]/ul[1]/li[@class=&quot;fields&quot;]/div[@class=&quot;input-box&quot;]/label[1]</value>
   </webElementProperties>
</WebElementEntity>
