<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>label_I AM OVER 18 AND I AGREE</name>
   <tag></tag>
   <elementGuidId>89861479-948c-4376-85df-c7a660a536bd</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value></value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>id(&quot;checkout-agreements&quot;)/ol[@class=&quot;checkout-agreements&quot;]/li[1]/label[1][count(. | //label[(text() = '
                            I AM OVER 18 AND I AGREE TO THE ABOVE CONDITIONS (INCLUDING THE OBLIGATION TO PAY IF I FAIL TO CANCEL MY SUBSCRIPTION) AND THE LES MILLS ON DEMAND TERMS OF USE.                        ' or . = '
                            I AM OVER 18 AND I AGREE TO THE ABOVE CONDITIONS (INCLUDING THE OBLIGATION TO PAY IF I FAIL TO CANCEL MY SUBSCRIPTION) AND THE LES MILLS ON DEMAND TERMS OF USE.                        ')]) = count(//label[(text() = '
                            I AM OVER 18 AND I AGREE TO THE ABOVE CONDITIONS (INCLUDING THE OBLIGATION TO PAY IF I FAIL TO CANCEL MY SUBSCRIPTION) AND THE LES MILLS ON DEMAND TERMS OF USE.                        ' or . = '
                            I AM OVER 18 AND I AGREE TO THE ABOVE CONDITIONS (INCLUDING THE OBLIGATION TO PAY IF I FAIL TO CANCEL MY SUBSCRIPTION) AND THE LES MILLS ON DEMAND TERMS OF USE.                        ')])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>label</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>for</name>
      <type>Main</type>
      <value>agreement-1</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                                                                                                    I AM OVER 18 AND I AGREE TO THE ABOVE CONDITIONS (INCLUDING THE OBLIGATION TO PAY IF I FAIL TO CANCEL MY SUBSCRIPTION) AND THE LES MILLS ON DEMAND TERMS OF USE.                        </value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;checkout-agreements&quot;)/ol[@class=&quot;checkout-agreements&quot;]/li[1]/label[1]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <value>//form[@id='checkout-agreements']/ol/li/label</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='You acknowledge that exercising to the programs on LES MILLS'])[1]/following::label[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Buy Now'])[1]/preceding::label[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <value>//label</value>
   </webElementXpaths>
</WebElementEntity>
