<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_CANCEL LES MILLS ON DEMAN</name>
   <tag></tag>
   <elementGuidId>6760c5a5-fc33-4154-bbc6-6665ad3d7362</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//span[(text() = 'CANCEL LES MILLS ON DEMAND' or . = 'CANCEL LES MILLS ON DEMAND')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>CANCEL LES MILLS ON DEMAND</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;top&quot;)/body[@class=&quot;lm-subscription-index-cancelsubscription customer-account&quot;]/div[@class=&quot;wrapper&quot;]/div[@class=&quot;page&quot;]/div[@class=&quot;main-container col2-left-layout&quot;]/div[@class=&quot;main&quot;]/div[@class=&quot;col-main&quot;]/div[@class=&quot;my-account&quot;]/div[@class=&quot;customer-subscription-cancel&quot;]/div[@class=&quot;subscription-cancel-buttons&quot;]/button[@class=&quot;cancel&quot;]/span[1]/span[1]</value>
   </webElementProperties>
</WebElementEntity>
