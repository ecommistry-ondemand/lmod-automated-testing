<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Sign in</name>
   <tag></tag>
   <elementGuidId>e3dd487b-4887-4147-9ff1-58e894efb8a6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>id(&quot;header-nav&quot;)/div[@class=&quot;nav-links&quot;]/ul[@class=&quot;right&quot;]/li[3]/a[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>https://www.lesmillsondemand.com/en_us/customer/account/login</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Sign in</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;header-nav&quot;)/div[@class=&quot;nav-links&quot;]/ul[@class=&quot;right&quot;]/li[3]/a[1]</value>
   </webElementProperties>
</WebElementEntity>
