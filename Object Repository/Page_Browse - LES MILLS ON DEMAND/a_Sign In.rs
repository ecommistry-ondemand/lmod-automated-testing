<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Sign In</name>
   <tag></tag>
   <elementGuidId>02ca3859-b361-4b3e-b00c-b1140f120021</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>/html[@class=&quot;no-js is-loaded wf-brandongrotesque-n4-active wf-brandongrotesque-i4-active wf-alternategothicno3d-n4-active wf-brandongrotesque-n5-active wf-brandongrotesque-n7-active wf-active&quot;]/body[@class=&quot;site-background-color   logged-out browse group-a&quot;]/header[@class=&quot;nav-background-color border-bottom site-border-color site-header has-logo&quot;]/div[@class=&quot;row full-width padding-top-medium padding-bottom-medium primary-row-container flex-container&quot;]/div[@class=&quot;hide-for-small-only text-right flex-right small-16 medium-5 columns small-only-text-center user-activity site-border-color&quot;]/div[@class=&quot;user-actions has-image padding-right-medium&quot;]/a[@class=&quot;flex-item user-action-link head site-link-header-color  site-font-primary-family&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/login?lesmillsondemanda=33bc94e0-7fb3-4ac7-a63b-9d155f97afe6</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>flex-item user-action-link head site-link-header-color  site-font-primary-family</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Sign In</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;no-js is-loaded wf-brandongrotesque-n4-active wf-brandongrotesque-i4-active wf-alternategothicno3d-n4-active wf-brandongrotesque-n5-active wf-brandongrotesque-n7-active wf-active&quot;]/body[@class=&quot;site-background-color   logged-out browse group-a&quot;]/header[@class=&quot;nav-background-color border-bottom site-border-color site-header has-logo&quot;]/div[@class=&quot;row full-width padding-top-medium padding-bottom-medium primary-row-container flex-container&quot;]/div[@class=&quot;hide-for-small-only text-right flex-right small-16 medium-5 columns small-only-text-center user-activity site-border-color&quot;]/div[@class=&quot;user-actions has-image padding-right-medium&quot;]/a[@class=&quot;flex-item user-action-link head site-link-header-color  site-font-primary-family&quot;]</value>
   </webElementProperties>
</WebElementEntity>
