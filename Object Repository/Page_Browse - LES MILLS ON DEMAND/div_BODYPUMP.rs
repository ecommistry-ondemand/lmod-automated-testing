<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_BODYPUMP</name>
   <tag></tag>
   <elementGuidId>8dddd6cc-ad0d-456e-add1-5ee6644b497d</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>site-font-primary-color truncate</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>BODYPUMP</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;browse&quot;)/div[1]/div[@class=&quot;browse-rows&quot;]/div[@class=&quot;row browse-row full-width programs&quot;]/div[@class=&quot;horizontal-row-container horizontal-row browse-rows__horizontal-rows--mobile-row-padding browse-rows__horizontal-rows--mobile-scrolling&quot;]/div[@class=&quot;slick-initialized slick-slider&quot;]/div[@class=&quot;slick-list&quot;]/div[@class=&quot;slick-track&quot;]/div[@class=&quot;slick-slide slick-active slick-slide__card-wrapper&quot;]/div[@class=&quot;card-enter horizontal-row-item&quot;]/div[@class=&quot;browse-item-card&quot;]/div[@class=&quot;site-font-primary-color browse-item-title&quot;]/a[1]/div[@class=&quot;subtext-container&quot;]/div[@class=&quot;site-font-primary-color truncate&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <value>//div[@id='browse']/div/div[2]/div[4]/div[2]/div/div/div/div[6]/div/div[2]/div/a/div[2]/div</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='My List'])[42]/following::div[6]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Add to My List ...'])[35]/following::div[6]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='BODYCOMBAT'])[3]/preceding::div[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Add to My List ...'])[36]/preceding::div[4]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <value>//div[4]/div[2]/div/div/div/div[6]/div/div[2]/div/a/div[2]/div</value>
   </webElementXpaths>
</WebElementEntity>
