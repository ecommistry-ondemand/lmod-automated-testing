<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_2. WHAT WORKOUT TO CHOOSE</name>
   <tag></tag>
   <elementGuidId>544b0824-4da7-40ac-86be-c681db3db14b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='browse']/div/div[2]/div[7]/div[2]/div/div/div/div[2]/div/div[2]/div/a/div[2]/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>site-font-primary-color truncate</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>2. WHAT WORKOUT TO CHOOSE</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;browse&quot;)/div[1]/div[@class=&quot;browse-rows&quot;]/div[@class=&quot;row browse-row full-width new-to-lmod&quot;]/div[@class=&quot;horizontal-row-container horizontal-row browse-rows__horizontal-rows--mobile-row-padding browse-rows__horizontal-rows--mobile-scrolling&quot;]/div[@class=&quot;slick-initialized slick-slider&quot;]/div[@class=&quot;slick-list&quot;]/div[@class=&quot;slick-track&quot;]/div[@class=&quot;slick-slide slick-active slick-slide__card-wrapper&quot;]/div[@class=&quot;card-enter horizontal-row-item&quot;]/div[@class=&quot;browse-item-card&quot;]/div[@class=&quot;site-font-primary-color browse-item-title&quot;]/a[1]/div[@class=&quot;subtext-container&quot;]/div[@class=&quot;site-font-primary-color truncate&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <value>//div[@id='browse']/div/div[2]/div[7]/div[2]/div/div/div/div[2]/div/div[2]/div/a/div[2]/div</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='What workout to chose? ...'])[1]/following::div[8]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='See all'])[6]/following::div[29]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='LEARN THE MOVES'])[1]/preceding::div[28]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='See all'])[7]/preceding::div[28]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <value>//div[7]/div[2]/div/div/div/div[2]/div/div[2]/div/a/div[2]/div</value>
   </webElementXpaths>
</WebElementEntity>
