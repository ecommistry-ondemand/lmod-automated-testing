<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_START YOUR FREE TRIAL</name>
   <tag></tag>
   <elementGuidId>09d9825a-0cbb-407f-ab93-e862a66ef0db</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>id(&quot;main&quot;)/section[@class=&quot;classhero classHero--workout classHero--with-benefits-bar classhero-with-benefits-bar__center&quot;]/div[@class=&quot;classhero__wrapper&quot;]/div[@class=&quot;classhero__body class-hero&quot;]/div[@class=&quot;classhero__inner classHero__inner--body class-hero-inner&quot;]/ul[@class=&quot;classhero__buttons&quot;]/li[@class=&quot;classhero__buttons__item&quot;]/a[@class=&quot;classhero__cta btn&quot;][count(. | //a[@href = 'https://www.lesmillsondemand.com/en_us/membership' and (text() = 'START YOUR FREE TRIAL' or . = 'START YOUR FREE TRIAL')]) = count(//a[@href = 'https://www.lesmillsondemand.com/en_us/membership' and (text() = 'START YOUR FREE TRIAL' or . = 'START YOUR FREE TRIAL')])]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>id(&quot;main&quot;)/section[@class=&quot;classhero classHero--workout classHero--with-benefits-bar classhero-with-benefits-bar__center&quot;]/div[@class=&quot;classhero__wrapper&quot;]/div[@class=&quot;classhero__body class-hero&quot;]/div[@class=&quot;classhero__inner classHero__inner--body class-hero-inner&quot;]/ul[@class=&quot;classhero__buttons&quot;]/li[@class=&quot;classhero__buttons__item&quot;]/a[@class=&quot;classhero__cta btn&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>https://www.lesmillsondemand.com/en_us/membership</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>target</name>
      <type>Main</type>
      <value>_blank</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>classhero__cta btn</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>START YOUR FREE TRIAL</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;main&quot;)/section[@class=&quot;classhero classHero--workout classHero--with-benefits-bar classhero-with-benefits-bar__center&quot;]/div[@class=&quot;classhero__wrapper&quot;]/div[@class=&quot;classhero__body class-hero&quot;]/div[@class=&quot;classhero__inner classHero__inner--body class-hero-inner&quot;]/ul[@class=&quot;classhero__buttons&quot;]/li[@class=&quot;classhero__buttons__item&quot;]/a[@class=&quot;classhero__cta btn&quot;]</value>
   </webElementProperties>
</WebElementEntity>
